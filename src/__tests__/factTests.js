import fact from '../store/modules/fact';

    const expected = 'Cats use their whiskers to the world around them in an effort to determine which small spaces they can fit into.';
    const actual = fact.state.factText;

    test ("expected should match actual", ()=>{
        expect(actual).toEqual(expected);
    })