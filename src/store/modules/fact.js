import axios from 'axios'

const state = {
  factText: 'Cats use their whiskers to the world around them in an effort to determine which small spaces they can fit into.'

}

const actions = {

  getFact: async ({ commit }) => {
    try {
      const resp = await axios.get('fact')
      commit('getFactSuccess', resp.data)
      return resp
    } catch (err) {
      return await Promise.reject(err)
    }
  }
}

const mutations = {
  getFactSuccess: (state, resp) => {
    state.factText = resp.fact
  }
}

export default {
  state,
  actions,
  mutations
}
